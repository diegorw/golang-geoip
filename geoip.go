package main

import (
    "encoding/json"
    "fmt"
    "io"
    "io/ioutil"
    "log"
    "net"
    "net/http"
    "os"
)

func main() {
    // Check command-line parameter existence
    if len(os.Args) != 2 {
        fmt.Printf("Usage: %s <ip>\n", os.Args[0])
        fmt.Printf("Example: %s 1.1.1.1\n", os.Args[0])
        os.Exit(1)
    }

    // Parsing of command-line parameter
    arg := net.ParseIP(os.Args[1])
    if nil == arg {
        s := fmt.Sprintln(os.Args[1], "is not a valid IP")
        io.WriteString(os.Stdout, s)
        os.Exit(1)
    }

    // IP-API Request handler
    resp, err := http.Get("http://ip-api.com/json/" + arg.String())
    if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }
    defer resp.Body.Close()

    // Response data handler
    htmlData, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }

    // JSON struct handler
    type GeoIP struct {
        Status      string
        Message     string
        Country     string
        CountryCode string
        ISP         string
    }
    var data GeoIP

    jsonErr := json.Unmarshal([]byte(string(htmlData)), &data)

    if jsonErr != nil {
        log.Fatal(jsonErr)
        os.Exit(1)
    }

    if data.Status == "fail" {
        fmt.Printf("Failed: %s\n", data.Message)
        os.Exit(1)
    }

    fmt.Printf("Country: %s (%s)\n", data.Country, data.CountryCode)
    fmt.Printf("ISP: %s\n", data.ISP)
}
